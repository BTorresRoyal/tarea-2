//Funcion generar numeros aleatorios 1-10
let random = () => Math.floor(Math.random() * 101);;

//Funcion de llamada a la api usando fetch()
async function getUsers() {
    let url = "https://amiiboapi.com/api/amiibo/";
    let res = await fetch(url);
    let data = await res.json();
    let arr = data.amiibo;

    //Mostrando datos en pantalla
    document.getElementById("carta").innerHTML = '<div id="cartases" class="card-deck"></div>';
    for (let i = 0; i < 3; i++) {
        //Obteniendo un objeto del arreglo
        let myObj = arr[random()];
        document.getElementById("cartases").innerHTML += ` 
        <div class="card" style="width:300px">
            <div class="card-body">
                <h4 class="card-title">${myObj.name}</h4>
                <p class="card-text">Game Series: ${myObj.gameSeries}</p>
                <br>
            </div>
            <img class="card-img-top" src="${myObj.image}" alt="Card image" style="width:100%">
        </div>
        `;
    }
    return data;
}
